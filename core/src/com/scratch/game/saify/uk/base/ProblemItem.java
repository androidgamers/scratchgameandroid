package com.scratch.game.saify.uk.base;

import java.util.Random;

/**
 * Created by Johars on 1/29/2016.
 */
public class ProblemItem {
    public static final int TURNLEFT=101;
    public static final int TURNRIGHT=102;
    public static final int TURNUP=103;
    public static final int TURNDOWN=104;
    public static final int MOVENOOFSTEPS=105;
    private Random random=new Random();
    private int problemCode=101;
    private String solution="Turn Left";
    public ProblemItem(int code)
    {
        problemCode=code;
        if(problemCode<101) {
            problemCode=101;
        }else if (problemCode>105) {
            problemCode=105;
        }
        MakeItem();
    }
    public ProblemItem()
    {
        problemCode=random.nextInt(6)+100;
        if(problemCode<101) {
            problemCode=101;
        }else if (problemCode>105) {
            problemCode=105;
        }
        MakeItem();
    }
    private void MakeItem()
    {
        switch (problemCode)
        {
            case TURNLEFT:
                solution="Turn Left";
                break;
            case TURNRIGHT:
                solution="Turn Right";
                break;
            case TURNUP:
                solution="Turn Up";
                break;
            case TURNDOWN:
                solution="Turn Down";
                break;
            case MOVENOOFSTEPS:
                solution="Steps: "+random.nextInt(5);
                break;
            default:
                solution="Turn Left";
                break;
        }
    }

    public String getSolution() {
        return solution;
    }
    public int getProblemCode() {
        return problemCode;
    }
    public int StepsIfPossible()
    {
        if(problemCode==105)
        {
            String str=solution.replace("Steps: ","");
            return Integer.parseInt(str);
        }else {
            return 0;
        }
    }
}
