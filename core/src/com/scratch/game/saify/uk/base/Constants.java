package com.scratch.game.saify.uk.base;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.scratch.game.saify.uk.ScratchGame;

/**
 * Created by Johars on 3/1/2016.
 */
public class Constants {
    public static final String MODE_EASY="Easy Mode";
    public static final String MODE_NORMAL="Normal Mode";
    public static final String MODE_HARD="Hard Mode";
    public static final String MODE_HARDEST="Hardest Mode";
    public static final String ACTION_EXECUTE="Execute";
    public static final String ACTION_PROBLEM="Problem";

    public static final float BUTTON_WIDTH = 300f;
    public static final float BUTTON_HEIGHT = 60f;
    public static final float BUTTON_SPACING = 10f;
    public static final int PRIME_TWO=2;
    public static final int PRIME_THREE=3;
    public static final int PRIME_FIVE=5;
    public static final int PRIME_SEVEN=7;
    public static final int PRIME_ELEVEN=11;
    public static final int PRIME_THIRTEEN=11;

    public static final TextButton getTextButton(String text,TextButton.TextButtonStyle textButtonStyle)
    {
        Drawable updrawable=new TextureRegionDrawable(GetTextureRegion("button.png"));
        Drawable downdrawable=new TextureRegionDrawable(GetTextureRegion("button.png"));
        textButtonStyle.down=downdrawable;
        textButtonStyle.up=updrawable;
        BitmapFont font24=new BitmapFont(Gdx.files.internal("myfont.fnt"));
        TextButton button = new TextButton(text, textButtonStyle);
        button.setSize(Constants.BUTTON_WIDTH,Constants.BUTTON_HEIGHT);
        return button;
    }
    public static final Stage getStage()
    {
        Stage stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        return stage;
    }
    public static TextButton.TextButtonStyle getTextButtonStyle(ScratchGame game)
    {
        TextButton.TextButtonStyle textButtonStyle = new TextButton.TextButtonStyle();
        textButtonStyle.font = game.font;
        return textButtonStyle;
    }
    public static void LoadBackGround(ScratchGame game,OrthographicCamera camera)
    {
        Texture backImage;
        backImage = new Texture(Gdx.files.internal("back1.jpg"));
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        game.batch.draw(backImage,0,0,camera.viewportWidth,camera.viewportHeight);
        game.batch.end();
    }
    public static TextureRegion GetLogo()
    {
        Texture walkSheet = new Texture(Gdx.files.internal("logo.png"));
        TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth(), walkSheet.getHeight());
        return tmp[0][0];
    }
    public static TextureRegion GetTextureRegion(String image)
    {
        Texture walkSheet = new Texture(Gdx.files.internal(image));
        TextureRegion[][] tmp = TextureRegion.split(walkSheet, walkSheet.getWidth(), walkSheet.getHeight());
        return tmp[0][0];
    }
}
