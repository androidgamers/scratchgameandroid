package com.scratch.game.saify.uk.base;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Johars on 1/29/2016.
 */
public class ProblemMaker {
    private List<ProblemItem> problemList;
    private int length=3;
    public ProblemMaker(int length)
    {
        if(length>3) {
            this.length = length;
        }
        problemList=new LinkedList<ProblemItem>();
        for (int i=0;i<this.length;i++)
        {
            AddItem();
        }
    }
    private void AddItem()
    {
        if(problemList.size()<=0)
        {
            problemList.add(new ProblemItem());
        }else {
            ProblemItem ddd=problemList.get(problemList.size()-1);
            if(ddd.getProblemCode()!=ProblemItem.MOVENOOFSTEPS) {
                problemList.add(new ProblemItem(ProblemItem.MOVENOOFSTEPS));
            }else {
                ProblemItem item=new ProblemItem();
                if (ddd.getProblemCode() != item.getProblemCode()) {
                    problemList.add(new ProblemItem());
                } else {
                    AddItem();
                }
            }
        }
    }
    public List<ProblemItem> getProblemList() {
        return problemList;
    }
}
