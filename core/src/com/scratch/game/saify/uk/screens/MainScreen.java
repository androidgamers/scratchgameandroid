package com.scratch.game.saify.uk.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.scratch.game.saify.uk.ScratchGame;
import com.scratch.game.saify.uk.base.Constants;
import com.scratch.game.saify.uk.base.GScreen;

public class MainScreen extends GScreen {
    private ScratchGame game;
    private OrthographicCamera camera;
    private Stage stage;
    private Image image;
    private Table table;
    private TextButton easy,normal,hard,hardest;

    public MainScreen(ScratchGame gam) {
        game = gam;
        camera = new OrthographicCamera();
        camera.setToOrtho(false,800,480);
        stage = Constants.getStage();
        image=new Image(Constants.GetLogo());
        table = new Table();
        table.setFillParent(true);
        easy = Constants.getTextButton(Constants.MODE_EASY,Constants.getTextButtonStyle(game));
        easy.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                TextButton bt = ((TextButton) actor);
                if (bt.getText().toString().equals(Constants.MODE_EASY)) {
                    game.setScreen(new PlayerScreen(game, Constants.MODE_EASY));
                }
            }
        });
        normal = Constants.getTextButton(Constants.MODE_NORMAL,Constants.getTextButtonStyle(game));
        normal.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                TextButton bt = ((TextButton) actor);
                if(bt.getText().toString().equals(Constants.MODE_NORMAL)) {
                    game.setScreen(new PlayerScreen(game, Constants.MODE_NORMAL));
                }
            }
        });
        hard = Constants.getTextButton(Constants.MODE_HARD,Constants.getTextButtonStyle(game));
        hard.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                TextButton bt= ((TextButton) actor);
                if(bt.getText().toString().equals(Constants.MODE_HARD)) {
                    game.setScreen(new PlayerScreen(game, Constants.MODE_HARD));
                }
            }
        });
        hardest = Constants.getTextButton(Constants.MODE_HARDEST,Constants.getTextButtonStyle(game));
        hardest.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                TextButton bt= ((TextButton) actor);
                if(bt.getText().toString().equals(Constants.MODE_HARDEST)) {
                    game.setScreen(new PlayerScreen(game, Constants.MODE_HARDEST));
                    game.dispose();
                }
            }
        });
        table.add(image).expandX().center().padBottom(16f);
        table.row();table.add(easy).padBottom(16f);
        table.row();table.add(normal).padBottom(16f);
        table.row();
        table.add(hard).padBottom(16f);
        table.row();table.add(hardest).padBottom(16f);
    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0.7f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        Constants.LoadBackGround(game, camera);
        stage.addActor(table);
        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        stage.dispose();
        game.dispose();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        stage.getViewport().update(width, height, true);
    }
}
