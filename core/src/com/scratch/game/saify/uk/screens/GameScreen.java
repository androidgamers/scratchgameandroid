package com.scratch.game.saify.uk.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.scratch.game.saify.uk.ScratchGame;
import com.scratch.game.saify.uk.base.Constants;
import com.scratch.game.saify.uk.base.GScreen;
import com.scratch.game.saify.uk.base.ProblemItem;
import com.scratch.game.saify.uk.base.ProblemMaker;
import com.scratch.game.saify.uk.characters.Player;
import com.scratch.game.saify.uk.utils.PathPlotter;

import java.util.Iterator;
import java.util.List;

public class GameScreen extends GScreen {
    final ScratchGame game;
    OrthographicCamera camera;
    private List<ProblemItem> problem;
    private Player player;
    private PathPlotter path;
    public GameScreen(final ScratchGame gam,List<ProblemItem> maker,List<ProblemItem> userInput) {
        this.game = gam;
        problem=maker;
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 800, 480);
        path=new PathPlotter(game.batch,maker);
        player=new Player(game.batch);
        player.create(camera.viewportWidth/2);
    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0.7f, 0.0f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        Constants.LoadBackGround(game, camera);
        path.create(camera.viewportWidth/2);
        path.render();
        player.render();
        MovePlayer();
    }
    @Override
    public void dispose() {
        game.dispose();
        player.dispose();
        path.dispose();
    }
    private void MovePlayer(){
        if(problem.size()>0)
        {
            ProblemItem item=problem.get(0);
            switch (item.getProblemCode())
            {
                case ProblemItem.TURNLEFT:
                    TurnLeft();
                    break;
                case ProblemItem.TURNRIGHT:
                    TurnRight();
                    break;
                case ProblemItem.TURNUP:
                    TurnUP();
                    break;
                case ProblemItem.TURNDOWN:
                    TurnDown();
                    break;
                case ProblemItem.MOVENOOFSTEPS:
                    MoveSteps(item.StepsIfPossible());
                    break;
            }
            problem.remove(0);
        }
    }
    private void TurnLeft()
    {
        player.direction=ProblemItem.TURNLEFT;
    }
    private void TurnRight()
    {
        player.direction=ProblemItem.TURNRIGHT;
    }
    private void TurnUP()
    {
        player.direction=ProblemItem.TURNUP;
    }
    private void TurnDown()
    {
        player.direction=ProblemItem.TURNDOWN;
    }
    private void MoveSteps(int steps)
    {
        switch (player.direction)
        {
            case ProblemItem.TURNUP:
                player.y += 200 * Gdx.graphics.getDeltaTime();
                break;
            case ProblemItem.TURNDOWN:
                player.y -= 200 * Gdx.graphics.getDeltaTime();
                break;
            case ProblemItem.TURNLEFT:
                player.x -= 200 * Gdx.graphics.getDeltaTime();
                break;
            case ProblemItem.TURNRIGHT:
                player.x += 200 * Gdx.graphics.getDeltaTime();
                break;
        }
    }
}
