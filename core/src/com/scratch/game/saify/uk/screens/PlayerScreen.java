package com.scratch.game.saify.uk.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.scratch.game.saify.uk.ScratchGame;
import com.scratch.game.saify.uk.base.Constants;
import com.scratch.game.saify.uk.base.GScreen;
import com.scratch.game.saify.uk.base.ProblemItem;
import com.scratch.game.saify.uk.base.ProblemMaker;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by Johars on 3/1/2016.
 */
public class PlayerScreen extends GScreen {
    private ScratchGame game;
    private String level;
    private Stage stage;
    private ProblemMaker maker;
    private List<ProblemItem> dummy;
    private List<ProblemItem> original;
    private List<ProblemItem> problemto;
    private List<ProblemItem> problemfrom;
    private OrthographicCamera camera;
    private Table tb;
    private ScrollPane fromscroll,toscroll;
    private Label fromlabel,tolabel;
    private Table from,to;
    private TextButton execute,probl;

    //Texture backImage;
    public PlayerScreen(final ScratchGame game,String level) {
        this.game=game;
        this.level=level;
        this.stage=Constants.getStage();
        camera = new OrthographicCamera();
        camera.setToOrtho(false,480,800);
        switch (this.level)
        {
            case Constants.MODE_EASY:
                maker=new ProblemMaker(8);
                break;
            case Constants.MODE_NORMAL:
                maker=new ProblemMaker(12);
                break;
            case Constants.MODE_HARD:
                maker=new ProblemMaker(20);
                break;
            case Constants.MODE_HARDEST:
                maker=new ProblemMaker(30);
                break;
        }
        problemfrom=new LinkedList<ProblemItem>();
        problemto=new LinkedList<ProblemItem>();
        original=new LinkedList<ProblemItem>();
        for (ProblemItem item:maker.getProblemList()) {
            original.add(item);
        }
        dummy=maker.getProblemList();
        Random random=new Random();
        while (dummy.size()!=0)
        {
            int a=random.nextInt(dummy.size());
            problemfrom.add(dummy.get(a));
            dummy.remove(a);
        }
        fromlabel=new Label("Commands",new Label.LabelStyle(this.game.font, Color.WHITE));
        tolabel=new Label("Ordered",new Label.LabelStyle(this.game.font, Color.WHITE));
        fromlabel.setFontScale(2.0f);
        tolabel.setFontScale(2.0f);
        execute=Constants.getTextButton(Constants.ACTION_EXECUTE,Constants.getTextButtonStyle(this.game));
        execute.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                if (maker.getProblemList().size() == problemto.size()) {
                    game.setScreen(new GameScreen(game, original, problemto));
                } else {
                    Dialog dialogs = new Dialog("Commands not in Order", new Window.WindowStyle(game.font, Color.RED, null));
                    dialogs.setModal(true);
                    dialogs.show(stage);
                }
            }
        });
        probl=Constants.getTextButton(Constants.ACTION_PROBLEM,Constants.getTextButtonStyle(this.game));
        probl.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                game.setScreen(new PathScreen(game, original, PlayerScreen.this));
            }
        });
    }
    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0.7f, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        Constants.LoadBackGround(game, camera);
        stage.clear();
        from = new Table();
        to=new Table();
        from.add(fromlabel);
        to.add(tolabel);




        for (final ProblemItem item:problemfrom) {
            final TextButton button=Constants.getTextButton(item.getSolution()+"",Constants.getTextButtonStyle(game));
            button.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    problemto.add(item);
                    problemfrom.remove(item);
                }
            });
            from.row();from.add(button);
        }
        for (final ProblemItem item:problemto) {
            final TextButton button=Constants.getTextButton(item.getSolution()+"",Constants.getTextButtonStyle(game));
            button.setHeight(50f);
            button.addListener(new ChangeListener() {
                @Override
                public void changed(ChangeEvent event, Actor actor) {
                    problemfrom.add(item);
                    problemto.remove(item);
                }
            });
            to.row();to.add(button);
        }
        fromscroll=new ScrollPane(from);
        toscroll=new ScrollPane(to);
        tb=new Table();
        tb.add(fromscroll).center();
        tb.add(toscroll).center();
        tb.row().pad(16f);
        tb.add(probl).bottom().left();
        tb.add(execute).bottom().right();
        tb.setFillParent(true);
        stage.addActor(tb);
        stage.draw();
    }
    @Override
    public void dispose() {
        super.dispose();
        game.dispose();
        stage.dispose();
    }
}