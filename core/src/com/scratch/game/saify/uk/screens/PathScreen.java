package com.scratch.game.saify.uk.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.scratch.game.saify.uk.ScratchGame;
import com.scratch.game.saify.uk.base.Constants;
import com.scratch.game.saify.uk.base.GScreen;
import com.scratch.game.saify.uk.base.ProblemItem;
import com.scratch.game.saify.uk.utils.PathPlotter;

import java.util.List;

/**
 * Created by Johars on 3/7/2016.
 */
public class PathScreen extends GScreen {
    private GScreen screen;
    private List<ProblemItem> items;
    private ScratchGame game;
    private OrthographicCamera camera;
    private PathPlotter path;
    private TextButton bt;
    private Stage stage;

    public PathScreen(ScratchGame game,List<ProblemItem> items,GScreen gScreen)
    {
        this.game=game;
        this.items=items;
        this.stage=Constants.getStage();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, 480, 800);
        this.screen=gScreen;
        path=new PathPlotter(game.batch,items);
        bt=Constants.getTextButton("Back",Constants.getTextButtonStyle(this.game));
        bt.padBottom(16f).padLeft(16f);
        bt.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                TextButton buu= ((TextButton) actor);
                if(buu.getText().toString().equals("Back")) {
                    setPrevScreen();
                }
            }
        });
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        Gdx.gl.glClearColor(0, 0.7f, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        Constants.LoadBackGround(game, camera);
        path.create(camera.viewportWidth / 2);
        path.render();
        stage.clear();
        stage.addActor(bt);
        stage.draw();
    }

    @Override
    public void dispose() {
        super.dispose();
        game.dispose();
        path.dispose();
        stage.dispose();
    }
    private void setPrevScreen()
    {
        game.setScreen(screen);
    }
}
