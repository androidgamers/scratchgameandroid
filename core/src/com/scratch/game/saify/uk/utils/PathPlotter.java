package com.scratch.game.saify.uk.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scratch.game.saify.uk.base.ProblemItem;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Johars on 3/4/2016.
 */
public class PathPlotter {
    private List<ProblemItem> problem;
    private SpriteBatch batch;
    private Texture walkSheet;
    private Texture walkSheet1;
    public float x,y;
    public int direction=103;

    public PathPlotter(SpriteBatch batch,List<ProblemItem> problem)
    {
        this.problem=problem;
        this.batch=batch;
        walkSheet=new Texture(Gdx.files.internal("ab_crate_f.png"));
        walkSheet1=new Texture(Gdx.files.internal("cratetex.png"));
    }
    public void create(float initialX)
    {
        x=initialX-40;
        y=0;
    }
    public void render()
    {
        int index=0;
        batch.begin();
        batch.draw(walkSheet1, x, y, 80, 80);
        if(problem.size()<=0) {}else {
            //problem.remove(0);
            for (ProblemItem item:problem) {
                switch (item.getProblemCode()) {
                    case ProblemItem.TURNRIGHT:
                        direction = ProblemItem.TURNRIGHT;
                        break;
                    case ProblemItem.TURNLEFT:
                        direction = ProblemItem.TURNLEFT;
                        break;
                    case ProblemItem.TURNUP:
                        direction = ProblemItem.TURNUP;
                        break;
                    case ProblemItem.TURNDOWN:
                        direction = ProblemItem.TURNDOWN;
                        break;
                    case ProblemItem.MOVENOOFSTEPS:
                        switch (direction) {
                            case ProblemItem.TURNRIGHT:
                                for (int i = 0; i < item.StepsIfPossible(); i++) {
                                    x = x + 80;
                                    batch.draw(walkSheet, x, y, 80, 80);
                                }
                                break;
                            case ProblemItem.TURNLEFT:
                                for (int i = 0; i < item.StepsIfPossible(); i++) {
                                    x = x - 80;
                                    batch.draw(walkSheet, x, y, 80, 80);
                                }
                                break;
                            case ProblemItem.TURNUP:
                                for (int i = 0; i < item.StepsIfPossible(); i++) {
                                    y = y + 80;
                                    batch.draw(walkSheet, x, y, 80, 80);
                                }
                                break;
                            case ProblemItem.TURNDOWN:
                                for (int i = 0; i < item.StepsIfPossible(); i++) {
                                    y = y - 80;
                                    batch.draw(walkSheet, x, y, 80, 80);
                                }
                                break;
                            default:
                                break;
                        }
                        break;
                }
            }
        }
        batch.end();
    }

    public void dispose()
    {
        batch.dispose();
        walkSheet.dispose();
        walkSheet1.dispose();
    }
}
