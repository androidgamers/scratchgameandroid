package com.scratch.game.saify.uk.characters;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.scratch.game.saify.uk.base.ProblemItem;

public class Player {
    private static final int FRAME_COLS = 4;
    private static final int FRAME_ROWS = 4;
    Animation upanim;
    Animation downanim;
    Animation leftanim;
    Animation rightanim;
    Texture walkSheet;
    TextureRegion[] dir_up;
    TextureRegion[] dir_down;
    TextureRegion[] dir_right;
    TextureRegion[] dir_left;
    SpriteBatch spriteBatch;
    TextureRegion currentFrame;
    float stateTime;
    public int direction=103;
    public float x=0;
    public float y=0;
    private TextureRegion[][] tmp;
    public Player(SpriteBatch spriteBatch)
    {
        this.spriteBatch=spriteBatch;
    }
    public void create(float centerX) {
        walkSheet = new Texture(Gdx.files.internal("serge.png"));
        tmp = TextureRegion.split(walkSheet, walkSheet.getWidth()/FRAME_COLS, walkSheet.getHeight()/FRAME_ROWS);
        dir_up = new TextureRegion[FRAME_COLS];
        dir_down = new TextureRegion[FRAME_COLS];
        dir_left = new TextureRegion[FRAME_COLS];
        dir_right = new TextureRegion[FRAME_COLS];
        int index;
        for(int i=0;i<FRAME_ROWS;i++) {
            index = 0;
            for (int j = 0; j < FRAME_COLS; j++) {
                switch (i)
                {
                    case 0:
                        dir_up[index++] = tmp[i][j];
                        break;
                    case 1:
                        dir_right[index++] = tmp[i][j];
                        break;
                    case 2:
                        dir_down[index++] = tmp[i][j];
                        break;
                    case 3:
                        dir_left[index++] = tmp[i][j];
                        break;
                }
            }
        }
        upanim = new Animation(0.025f, dir_up);
        downanim = new Animation(0.025f, dir_down);
        leftanim = new Animation(0.025f, dir_left);
        rightanim = new Animation(0.025f, dir_right);
        stateTime = 0f;
        x=centerX;
    }
    public void render() {
        stateTime += Gdx.graphics.getDeltaTime();
        switch (direction)
        {
            case ProblemItem.TURNLEFT:
                currentFrame = leftanim.getKeyFrame(stateTime, true);
                break;
            case ProblemItem.TURNRIGHT:
                currentFrame = rightanim.getKeyFrame(stateTime, true);
                break;
            case ProblemItem.TURNDOWN:
                currentFrame = downanim.getKeyFrame(stateTime, true);
                break;
            case ProblemItem.TURNUP:
                currentFrame = upanim.getKeyFrame(stateTime, true);
                break;
            default:
                currentFrame = upanim.getKeyFrame(stateTime, true);
                break;
        }
        spriteBatch.begin();
        spriteBatch.draw(currentFrame, x-32, y,64,64);
        spriteBatch.end();
    }
    public void dispose()
    {
        spriteBatch.dispose();
        walkSheet.dispose();

    }
}
