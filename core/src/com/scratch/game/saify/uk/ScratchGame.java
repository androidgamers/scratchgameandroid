package com.scratch.game.saify.uk;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.scratch.game.saify.uk.screens.MainScreen;

public class ScratchGame extends Game{
	public SpriteBatch batch;
	public BitmapFont font;
	@Override
	public void create() {
		batch = new SpriteBatch();
        font = new BitmapFont(Gdx.files.internal("myfont.fnt"));
        this.setScreen(new MainScreen(this));
	}
	@Override
	public void render() {
        super.render();
	}

	@Override
	public void dispose() {
		batch.dispose();
        font.dispose();
	}
}
